module.exports = {
  trailingSlash: true,
  i18n: {
    locales: ['en-US', 'sv-SE'],
    defaultLocale: 'sv-SE',
    localeDetection: false
  }
};
